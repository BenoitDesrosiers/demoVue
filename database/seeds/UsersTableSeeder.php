<?php
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        factory(App\User::class)->create(['name'=>'admin', 'email'=>'admin@chose.com','password'=>bcrypt('admin')]);
        factory(App\User::class, 10)->create();
    }
}

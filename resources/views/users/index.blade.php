@extends('layouts.app')

@section('titre', 'Usagers')

@section('content')
    <a href="{{ route('users.create') }}"><button type="button" class="btn btn-raised btn-primary">Ajouter un usager</button></a>
    <table class="table">
        <thead>
        <tr>
            <th>Identifiant</th>
            <th>Nom d'utilisateur</th>
            <th>Courriel</th>
        </tr>
        </thead>
        <tbody>
        @foreach($usagers as $usager)
            <tr>

                <td><a href="{{ route('users.show', $usager->id) }}">{{ $usager->id }}</a> </td>
                <td>{{ $usager->name }} </td>
                <td>{{ $usager->email  }} </td>

            </tr>
        @endforeach
        </tbody>
    </table>
@endsection